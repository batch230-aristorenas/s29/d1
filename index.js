// Advance Query Operators
	- we want more flexible querying of data within MongoDB

//[SECTION] Comparison Query Operators

	// $gt - greater than
	// $gte - greater than or equal

/*
	Syntax:
		- db.collectionName.find({field: {$gt/$gte: value}});
*/

db.users.find(
		{
			age : {$gt: 21}
		}

	)

db.users.find(
		{
			age : {$gte: 21}
		}

	)

// $lt - less than
// $lte - less than or equal to

db.users.find(
		{
			age : {
				$lt: 82
			}
		}

	)

db.users.find(
		{
			age : {
				$lte: 82
			}
		}

	)


// $ne - not equal

db.users.find({age: {$ne: 82}})


// $in (good for multiple element in 1 field)
/*
	-allows us to find docs with specific math criteria with one field using different values

	Syntax:
		- db.collectionName.find(field: {$in: [valueA, valueB]})
*/


db.users.find(
		{
			lastName:{
				$in: ['Hawking', 'Doe']
			}	
		}
	)


db.users.find( 
		{
			courses:{
				$in: ['HTML', 'React']
			}	
		}
	)


// $or (good for multiple field)
/*
	Syntax:
	db.collectionName.find({$or[{fieldA:valueA}, {fieldB:valueB}]})
*/

db.users.find(
	{
		$or:[
			{firstName : "Neil"},
			{age : 21}
			]
	}
);


// $and (all should be satidfied)
/*
	Syntax:
	db.collectionName.find({$and:[{fieldA:valueA}, {fieldB:valueB}]})
*/

db.users.find(
		{
			$and: [
					{age: {$ne:82}},
					{age: {$ne:76}}
				]
		}

	)



// [SECTION] Field Projection
// to help with the readability of the values returned, we can include/exclude fields from the response
/*
	db.collectionName.find({criteria}, {fields you want to include, it shld have a value of 1})
*/

// Inclusion (1)
db.users.find(
	{
		//criteria
		firstName: "Jane"
	},
	{
		firstName:1,
		lastName:1,
		contact:1
	}
)


// Exclusion (0)

db.users.find(
	{
		//criteria
		firstName: "Jane"
	},
	{
		contact:0,
		department:0
	}
)


// Supressing the ID field

// not suggestable

db.users.find(
	{
		//criteria
		firstName: "Jane"
	},
	{
		firstName:1,
		lastName:1,
		contact:1,
		department:0
	}

)


// $regex (same as suggestions)
// case sensitive

db.users.find(
	{
		firstName : {
			$regex: "N"
		}
	}

)


// Not case sensitive


db.users.find(
	{
		firstName : {
			$regex: "N", $options: "$i"
		}
	}

)